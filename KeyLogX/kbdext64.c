//*******************************************************************
// Program: kbdext.c
// Source files: kbdext.c kbdext.h
// Author: Marc-Andre Moreau
// Last update: September 18th, 2008
// Description: Replacement API for Microsoft's ToUnicode() function
// You should load the current keyboard layout with loadKeyboardLayout()
// before calling convertVirtualKeyToWChar()
//*******************************************************************

#include "kbdext64.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>



typedef PKBDTABLES64(CALLBACK* KbdLayerDescriptor64)(VOID);
int getKeyboardLayoutFile64(char* layoutFile, DWORD bufferSize, HKL hklLayout);
PVK_TO_WCHARS641 pVkToWchars641 = NULL;
PVK_TO_WCHARS642 pVkToWchars642 = NULL;
PVK_TO_WCHARS643 pVkToWchars643 = NULL;
PVK_TO_WCHARS644 pVkToWchars644 = NULL;
PVK_TO_WCHARS645 pVkToWchars645 = NULL;
PVK_TO_WCHARS646 pVkToWchars646 = NULL;
PVK_TO_WCHARS647 pVkToWchars647 = NULL;
PVK_TO_WCHARS648 pVkToWchars648 = NULL;
PVK_TO_WCHARS649 pVkToWchars649 = NULL;
PVK_TO_WCHARS6410 pVkToWchars6410 = NULL;
PMODIFIERS64 pCharModifiers64;
PDEADKEY64 pDeadKey64;

HINSTANCE loadKeyboardLayout64(HKL keyLayout)
{
	PKBDTABLES64 pKbd;
	HINSTANCE kbdLibrary;
	KbdLayerDescriptor64 pKbdLayerDescriptor64 = NULL;

	char layoutFile[MAX_PATH];
	if(getKeyboardLayoutFile64(layoutFile, sizeof(layoutFile), keyLayout) == -1)
		return NULL;

	char systemDirectory[MAX_PATH];
	GetSystemDirectory(systemDirectory, MAX_PATH);

	char kbdLayoutFilePath[MAX_PATH];
	_snprintf(kbdLayoutFilePath, MAX_PATH, "%s\\%s", systemDirectory, layoutFile);

	kbdLibrary = LoadLibrary(kbdLayoutFilePath);

	pKbdLayerDescriptor64 = (KbdLayerDescriptor64)GetProcAddress(kbdLibrary, "KbdLayerDescriptor");

	if(pKbdLayerDescriptor64 != NULL)
		pKbd = (PKBDTABLES64)pKbdLayerDescriptor64();
	else
		return NULL;

	int i = 0;
	do
	{
		INIT_PVK_TO_WCHARS64(i, 1)
		INIT_PVK_TO_WCHARS64(i, 2)
		INIT_PVK_TO_WCHARS64(i, 3)
		INIT_PVK_TO_WCHARS64(i, 4)
		INIT_PVK_TO_WCHARS64(i, 5)
		INIT_PVK_TO_WCHARS64(i, 6)
		INIT_PVK_TO_WCHARS64(i, 7)
		INIT_PVK_TO_WCHARS64(i, 8)
		INIT_PVK_TO_WCHARS64(i, 9)
		INIT_PVK_TO_WCHARS64(i, 10)
		i++;
	}
	while(pKbd->pVkToWcharTable[i].cbSize != 0);

	pCharModifiers64 = (PMODIFIERS64)(pKbd->pCharMODIFIERS64);
	pDeadKey64 = (PDEADKEY64)pKbd->pDEADKEY64;

	return kbdLibrary;
}

int unloadKeyboardLayout64(HINSTANCE kbdLibrary)
{
	if(kbdLibrary != 0)
		return (FreeLibrary(kbdLibrary) != 0);
	else
		return 0;
}

int convertVirtualKeyToWChar64(int virtualKey, PWCHAR outputChar, PWCHAR deadChar)
{
	int i = 0;
	short state = 0;
	int shift = -1;
	int mod = 0;
	int charCount = 0;

	WCHAR baseChar;
	WCHAR diacritic;
	*outputChar = 0;

	int capsLock = (GetKeyState(VK_CAPITAL) & 0x1);

	do
	{
		state = GetAsyncKeyState(pCharModifiers64->pVkToBit[i].Vk);

		if(pCharModifiers64->pVkToBit[i].Vk == VK_SHIFT)
		shift = i + 1; // Get modification number for Shift key

		if(state & ~SHRT_MAX)
		{
			if(mod == 0)
				mod = i + 1;
			else
				mod = 0; // Two modifiers at the same time!
		}
		i++;
	}
	while(pCharModifiers64->pVkToBit[i].Vk != 0);

	SEARCH_VK_IN_CONVERSION_TABLE64(1)
	SEARCH_VK_IN_CONVERSION_TABLE64(2)
	SEARCH_VK_IN_CONVERSION_TABLE64(3)
	SEARCH_VK_IN_CONVERSION_TABLE64(4)
	SEARCH_VK_IN_CONVERSION_TABLE64(5)
	SEARCH_VK_IN_CONVERSION_TABLE64(6)
	SEARCH_VK_IN_CONVERSION_TABLE64(7)
	SEARCH_VK_IN_CONVERSION_TABLE64(8)
	SEARCH_VK_IN_CONVERSION_TABLE64(9)
	SEARCH_VK_IN_CONVERSION_TABLE64(10)

	if(*deadChar != 0) // I see dead characters...
	{
		i = 0;
		do
		{
			baseChar = (WCHAR)pDeadKey64[i].dwBoth;
			diacritic = (WCHAR)(pDeadKey64[i].dwBoth >> 16);

			if((baseChar == *outputChar) && (diacritic == *deadChar))
			{
				*deadChar = 0;
				*outputChar = (WCHAR)pDeadKey64[i].wchComposed;
			}
			i++;
		}
		while(pDeadKey64[i].dwBoth != 0);
	}

	return charCount;
}

int getKeyboardLayoutFile64(char* layoutFile, DWORD bufferSize, HKL hklLayout)
{
	HKEY hKey;
	DWORD varType = REG_SZ;
	char kbdName[KL_NAMELENGTH];
	ActivateKeyboardLayout(hklLayout, KLF_SETFORPROCESS);
	GetKeyboardLayoutName(kbdName);
	char kbdKeyPath[51 + KL_NAMELENGTH];
	_snprintf(kbdKeyPath, 51 + KL_NAMELENGTH,
		"SYSTEM\\CurrentControlSet\\Control\\Keyboard Layouts\\%s", kbdName);

	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)kbdKeyPath, 0, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
        	return -1;

	if(RegQueryValueEx(hKey, (LPCSTR)"Layout File", NULL, &varType, (LPBYTE)layoutFile, &bufferSize) != ERROR_SUCCESS)
		return -1;

	RegCloseKey(hKey);

	return 1;
}