//*******************************************************************
// Program: kbdext.c
// Source file: kbdext.h
// Author: Marc-Andre Moreau
// Last update: September 16th, 2008
// Description: Header File containing function prototypes as well as
// many definitions and macros for extended keyboard function. Some
// parts were taken directly from Microsoft's kbd.h header file that
// is shipped with the Windows Driver Development Kit. It was an
// inconvenient to download and install the whole Windows DDK only
// for kbd.h so I just copied everything that was needed here.
//*******************************************************************

//#ifndef _KBD_EXT_
//#define _KBD_EXT_



#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>



HINSTANCE loadKeyboardLayout64(HKL keyLayout);
int unloadKeyboardLayout64(HINSTANCE kbdLibrary);
//int getKeyboardLayoutFile64(char* layoutFile, DWORD bufferSize,HKL hklLayout);
int convertVirtualKeyToWChar64(int virtualKey, PWCHAR out, PWCHAR buffer);

#if defined(BUILD_WOW6432)
    #define KBD_LONG_POINTER __ptr64
#else
    #define KBD_LONG_POINTER
#endif


#define KBD_LONG_POINTER64 __ptr64

#define CAPLOK 0x01
#define WCH_NONE 0xF000
#define WCH_DEAD 0xF001


typedef struct {
	BYTE Vk;
	BYTE ModBits;
} VK_TO_BIT64, *KBD_LONG_POINTER64 PVK_TO_BIT64;

typedef struct {
	PVK_TO_BIT64 pVkToBit;
	WORD       wMaxModBits;
	BYTE       ModNumber[];
} MODIFIERS64, *KBD_LONG_POINTER64 PMODIFIERS64;

typedef struct _VSC_VK64 {
	BYTE Vsc;
	USHORT Vk;
} VSC_VK64, *KBD_LONG_POINTER64 PVSC_VK64;

typedef struct _VK_VSC64 {
	BYTE Vk;
	BYTE Vsc;
} VK_VSC64, *KBD_LONG_POINTER64 PVK_VSC64;

#define TYPEDEF_VK_TO_WCHARS64(n) typedef struct _VK_TO_WCHARS64##n {  \
                                    BYTE  VirtualKey;      \
                                    BYTE  Attributes;      \
                                    WCHAR wch[n];          \
                                } VK_TO_WCHARS64##n, *KBD_LONG_POINTER64 PVK_TO_WCHARS64##n;

TYPEDEF_VK_TO_WCHARS64(1) // VK_TO_WCHARS641, *PVK_TO_WCHARS641;
TYPEDEF_VK_TO_WCHARS64(2) // VK_TO_WCHARS642, *PVK_TO_WCHARS642;
TYPEDEF_VK_TO_WCHARS64(3) // VK_TO_WCHARS643, *PVK_TO_WCHARS643;
TYPEDEF_VK_TO_WCHARS64(4) // VK_TO_WCHARS644, *PVK_TO_WCHARS644;
TYPEDEF_VK_TO_WCHARS64(5) // VK_TO_WCHARS645, *PVK_TO_WCHARS645;
TYPEDEF_VK_TO_WCHARS64(6) // VK_TO_WCHARS646, *PVK_TO_WCHARS645;
TYPEDEF_VK_TO_WCHARS64(7) // VK_TO_WCHARS647, *PVK_TO_WCHARS647;
// these three (8,9,10) are for FE
TYPEDEF_VK_TO_WCHARS64(8) // VK_TO_WCHARS648, *PVK_TO_WCHARS648;
TYPEDEF_VK_TO_WCHARS64(9) // VK_TO_WCHARS649, *PVK_TO_WCHARS649;
TYPEDEF_VK_TO_WCHARS64(10) // VK_TO_WCHARS6410, *PVK_TO_WCHARS6410;

typedef struct _VK_TO_WCHAR_TABLE64 {
	PVK_TO_WCHARS641 pVkToWchars;
	BYTE           nModifications;
	BYTE           cbSize;
} VK_TO_WCHAR_TABLE64, *KBD_LONG_POINTER64 PVK_TO_WCHAR_TABLE64;

typedef struct {
	DWORD  dwBoth;  // diacritic & char
	WCHAR  wchComposed;
	USHORT uFlags;
} DEADKEY64, *KBD_LONG_POINTER64 PDEADKEY64;

#define TYPEDEF_LIGATURE64(n) typedef struct _LIGATURE64##n {     \
                                    BYTE  VirtualKey;         \
                                    WORD  ModificationNumber; \
                                    WCHAR wch[n];             \
                                } LIGATURE64##n, *KBD_LONG_POINTER64 PLIGATURE64##n;

/*
* Table element types (for various numbers of LIGATURE64s), used
* to facilitate static initializations of tables.
*
* LIGATURE641 and PLIGATURE641 are used as the generic type
*/
TYPEDEF_LIGATURE64(1) // LIGATURE641, *PLIGATURE641;
TYPEDEF_LIGATURE64(2) // LIGATURE642, *PLIGATURE642;
TYPEDEF_LIGATURE64(3) // LIGATURE643, *PLIGATURE643;
TYPEDEF_LIGATURE64(4) // LIGATURE644, *PLIGATURE644;
TYPEDEF_LIGATURE64(5) // LIGATURE645, *PLIGATURE645;

typedef struct {
	BYTE   vsc;
	WCHAR *KBD_LONG_POINTER64 pwsz;
} VSC_LPWSTR64, *KBD_LONG_POINTER64 PVSC_LPWSTR64;

typedef WCHAR *KBD_LONG_POINTER64 DEADKEY64_LPWSTR;

/***************************************************************************\
* KBDTABLES64
*
* This structure describes all the tables that implement the keyboard layer.
*
* When switching to a new layer, we get a new KBDTABLES64 structure: all key
* processing tables are accessed indirectly through this structure.
*
\***************************************************************************/

typedef struct tagKbdLayer64 {
	/*
	* Modifier keys
	*/
	PMODIFIERS64 pCharMODIFIERS64;

	/*
	* Characters
	*/
	PVK_TO_WCHAR_TABLE64 pVkToWcharTable;  // ptr to tbl of ptrs to tbl

	/*
	* Diacritics
	*/
	PDEADKEY64 pDEADKEY64;

	/*
	* Names of Keys
	*/
	PVSC_LPWSTR64 pKeyNames;
	PVSC_LPWSTR64 pKeyNamesExt;
	WCHAR *KBD_LONG_POINTER64 *KBD_LONG_POINTER64 pKeyNamesDead;

	/*
	* Scan codes to Virtual Keys
	*/
	USHORT  *KBD_LONG_POINTER64 pusVSCtoVK;
	BYTE    bMaxVSCtoVK;
	PVSC_VK64 pVSCtoVK_E0;  // Scancode has E0 prefix
	PVSC_VK64 pVSCtoVK_E1;  // Scancode has E1 prefix

	/*
	* Locale-specific special processing
	*/
	DWORD fLocaleFlags;

	/*
	* LIGATURE64s
	*/
	BYTE       nLgMax;
	BYTE       cbLgEntry;
	PLIGATURE641 pLIGATURE64;

#if (NTDDI_VERSION >= NTDDI_WINXP)

	/*
	* Type and subtype. These are optional.
	*/
	DWORD      dwType;     // Keyboard Type
	DWORD      dwSubType;  // Keyboard SubType: may contain OemId

#endif

} KBDTABLES64, *KBD_LONG_POINTER64 PKBDTABLES64;


/*
* FarEast-specific special...
*/
typedef struct _VK_FUNCTION_PARAM64 {
	BYTE  NLSFEProcIndex;
	ULONG NLSFEProcParam;
} VK_FPARAM64, *KBD_LONG_POINTER64 PVK_FPARAM64;

typedef struct _VK_TO_FUNCTION_TABLE64 {
	BYTE Vk;
	BYTE NLSFEProcType;
	BYTE NLSFEProcCurrent;
	// Index[0] : Base
	// Index[1] : Shift
	// Index[2] : Control
	// Index[3] : Shift+Control
	// Index[4] : Alt
	// Index[5] : Shift+Alt
	// Index[6] : Control+Alt
	// Index[7] : Shift+Control+Alt
	BYTE NLSFEProcSwitch;   // 8 bits
	VK_FPARAM64 NLSFEProc[8];
	VK_FPARAM64 NLSFEProcAlt[8];
} VK_F64, *KBD_LONG_POINTER64 PVK_F64;

typedef struct tagKbdNlsLayer64 {
	USHORT OEMIdentifier;
	USHORT LayoutInformation;
	UINT  NumOfVkToF;
	PVK_F64 pVkToF;
	//
	// The pusMouseVKey array provides a translation from the virtual key
	// value to an index.  The index is used to select the appropriate
	// routine to process the virtual key, as well as to select extra
	// information that is used by this routine during its processing.
	// If this value is NULL, following default will be used.
	//
	// ausMouseVKey[] = {
	//     VK_CLEAR,           // Numpad 5: Click active button
	//     VK_PRIOR,           // Numpad 9: Up & Right
	//     VK_NEXT,            // Numpad 3: Down & Right
	//     VK_END,             // Numpad 1: Down & Left
	//     VK_HOME,            // Numpad 7: Up & Left
	//     VK_LEFT,            // Numpad 4: Left
	//     VK_UP,              // Numpad 8: Up
	//     VK_RIGHT,           // Numpad 6: Right
	//     VK_DOWN,            // Numpad 2: Down
	//     VK_INSERT,          // Numpad 0: Active button down
	//     VK_DELETE,          // Numpad .: Active button up
	//     VK_MULTIPLY,        // Numpad *: Select both buttons
	//     VK_ADD,             // Numpad +: Double click active button
	//     VK_SUBTRACT,        // Numpad -: Select right button
	//     VK_DEVIDE|KBDEXT,   // Numpad /: Select left button
	//     VK_NUMLOCK|KBDEXT}; // Num Lock
	//
	INT     NumOfMouseVKey;
	USHORT *KBD_LONG_POINTER64 pusMouseVKey;
} KBDNLSTABLES64, *KBD_LONG_POINTER64 PKBDNLSTABLES64;

#if (NTDDI_VERSION >= NTDDI_WINXP)
/*
* The maximum number of layout tables in a DLL
*/
#define KBDTABLE_MULTI64_MAX  (8)

/*
* Multiple keyboard layout table in a DLL
*/

// Extended macros

#define INIT_PVK_TO_WCHARS64(i, n) \
if((pKbd->pVkToWcharTable[i].cbSize - 2) / 2 == n) \
	pVkToWchars64##n = (PVK_TO_WCHARS64##n)pKbd->pVkToWcharTable[i].pVkToWchars; \

#define SEARCH_VK_IN_CONVERSION_TABLE64(n) \
i = 0; \
if(pVkToWchars64##n && (mod < n)) \
{ \
	do \
		{ \
		if(pVkToWchars64##n[i].VirtualKey == virtualKey) \
				{ \
			if((pVkToWchars64##n[i].Attributes == CAPLOK) && capsLock) { \
				if(mod == shift) mod = 0; else mod = shift; } \
			*outputChar = pVkToWchars64##n[i].wch[mod]; \
			charCount = 1; \
			if(*outputChar == WCH_NONE) { charCount = 0; } \
						else if(*outputChar == WCH_DEAD) \
			{ \
				*deadChar = pVkToWchars64##n[i + 1].wch[mod]; \
				charCount = 0; \
			} \
			break;\
				} \
		i++; \
		} \
			while(pVkToWchars64##n[i].VirtualKey != 0); \
} \

#endif // _KBD_EXT_
//#endif // _KBD64_